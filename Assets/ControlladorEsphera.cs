using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlladorEsphera : MonoBehaviour
{
    public float speed;

    int count_puntos;

    int contador_colision;

    public Text puntos;
    Color _orange = new Color(1.0f, 0.64f, 0.0f);
    public Text resultado;
    void Start() {
        count_puntos = 0;
        contador_colision=0;
        updateCounter();
    }

    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(horizontal, 0, vertical);

        GetComponent<Rigidbody>().AddForce(direction * speed);
    }


    void OnCollisionEnter(Collision myCol )
    {

        if (myCol.gameObject.name == "Enemigo")
        {
            contador_colision++;
            if (contador_colision == 1)
            {
                gameObject.GetComponent<Renderer>().material.color = Color.yellow;
            }
            if (contador_colision == 2)
            {
                gameObject.GetComponent<Renderer>().material.color = _orange;
            }
            if (contador_colision == 3)
            {
                gameObject.GetComponent<Renderer>().material.color = Color.red;
            }
           if (contador_colision == 4)
            {
                Destroy(gameObject);
                resultado.text = "HAS PERDIDO";
            }
            
        }
        if(myCol.gameObject.tag == "Prize")
        {
            count_puntos++;
            Destroy(myCol.gameObject);
            updateCounter();
        }



    }
    void updateCounter()
    {
        puntos.text = "Puntos: " + count_puntos + "/2";
        if (count_puntos >= 2)
        {
            resultado.text = "HAS GANADO";

        }
    }

}
